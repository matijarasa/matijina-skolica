from django.apps import AppConfig


class MatijaSkolicaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'matija_skolica'
